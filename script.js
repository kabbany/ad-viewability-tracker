/************************************************************************************************
 *                                                                                              *
 *                              VARIABLES DECLARATION                                           *
 *                                                                                              *
 ************************************************************************************************/
var adIsViewable = true,
  viewabilityTime = 0,
  adElement = document.getElementById("ad"),
  viewableSince = 0, //Timestamp to record since when the ad is viewable
  timeOffset = 0, //Storing the viewabilityTime in seconds when ad is not viewable
  adClicks = 0, //Number of clicks on the Ad
  adArea, //adArea in pixels is used for visibilty percentage calculations
  adVisibilityPercentage; //Track the visibilty in percentage of the Ad

/**
 * Logs the viewability values in the console
 *
 * @override
 */
window.log = function () {
  if(adIsViewable && viewableSince){
    viewabilityTime = timeOffset + (+new Date() - viewableSince) / 1000;
  }
  else{
    timeOffset = viewabilityTime;
  }
  console.log("Ad is viewable: ", adIsViewable, "\nViewability time of the ad in sec:", viewabilityTime);
  console.log("Ad has been clicked: ", adClicks);
  console.log("Ad visibilty percentage: ", adVisibilityPercentage, "%");
};

/************************************************************************************************
 *                                                                                              *
 *                              YOUR IMPLEMENTATION                                             *
 *                                                                                              *
 ************************************************************************************************/

adArea = adElement.clientWidth * adElement.clientHeight;

var isAdViewable = function(pageHasFocus){
  var adBoundingRect = adElement.getBoundingClientRect();
  var isPageVisible = typeof pageHasFocus === 'undefined' ? true : pageHasFocus;

  var clientHeight =
    window.innerHeight || document.documentElement.clientHeight;
  var clientWidth =
    window.innerWidth || document.documentElement.clientWidth;

  var top = adBoundingRect.top;
  var left = adBoundingRect.left;
  var bottom = adBoundingRect.bottom;
  var right = adBoundingRect.right;

  var currentViewableDimensions = {
    top: top >= 0 ? top : 0,
    right: right <= clientWidth ? right : clientWidth,
    left: left >= 0 ? left : 0,
    bottom: bottom <= clientHeight ? bottom : clientHeight,
  }
  var currentAdWidth =
    currentViewableDimensions.right - currentViewableDimensions.left;
  var currentAdHeight =
    currentViewableDimensions.bottom - currentViewableDimensions.top;
  var currentVisibleAdArea = currentAdWidth * currentAdHeight;
  adVisibilityPercentage = (currentVisibleAdArea / adArea) * 100;

  /*
  // Using the following condition to set adIsViewable, without adVisibilityPercentage
  adIsViewable = isPageVisible
    && top >= 0
    && left >= 0
    && bottom <= clientHeight
    && right <= clientWidth;
  */

  // Using adVisibilityPercentage to set adIsViewable instead of the previous condition
  adIsViewable = isPageVisible && adVisibilityPercentage === 100;

  // Set the viewableSince with the current timestamp if the ad starts to be
  // viewable
  if(viewableSince === 0 && adIsViewable){
    viewableSince = +new Date();
  }

  // Unset the viewableSince if the ad is not viewable anymore and update
  // the timeOffset with the latest viewabilityTime
  if(viewableSince !== 0 && !adIsViewable){
    timeOffset += (+new Date() - viewableSince) / 1000;
    viewableSince = 0;
  }
};

// Check the viewability of the ad when script starts
isAdViewable();

/**
  check the viewability of the ad on the following events:
  blur: to detect when window loses focus
  focus: to detect when window gains focus
  resize: to detect if the element is 100% viewable after resize
  scroll: to detect if the element is 100% viewable after scroll
**/
window.addEventListener('blur', function(){
  //pageHasFocus is false since the page is not focused
  isAdViewable(false);
});
window.addEventListener('focus', function(){
  //pageHasFocus is true since the page is focused
  isAdViewable(true);
});
window.addEventListener('resize', isAdViewable);
window.addEventListener('scroll', isAdViewable);

//Track clicks on the ad
adElement.addEventListener('click', function(){
  adClicks++;
});
